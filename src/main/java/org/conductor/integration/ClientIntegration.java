package org.conductor.integration;

import org.conductor.bootstrap.IComponentGroupBootstrap;
import org.conductor.component.events.ErrorEventListener;
import org.conductor.component.types.IComponent;
import org.conductor.database.Database;
import org.conductor.integration.factories.BootstrapFactory;
import org.conductor.util.StringUtil;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ClientIntegration implements IClientIntegration {
    private static final Logger log = LoggerFactory.getLogger(ClientIntegration.class);
    private Database database;
    private Map<String, Map<String, Object>> componentsOptions = new HashMap<String, Map<String, Object>>();
    private Map<String, IComponent> components = new HashMap<String, IComponent>();
    private Method onPropertyValueUpdated;
    private Method reportError;
    private Object caller;

    public ClientIntegration(Object caller, Method onPropertyValueUpdated, Method reportError) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        log.info("Initializing component group client.");
        this.database = new Database();
        this.caller = caller;
        this.onPropertyValueUpdated = onPropertyValueUpdated;
        this.reportError = reportError;
    }

    public void start(Map<String, Object> componentGroupOptions) throws Exception {
        log.info("Starting component group client.");
        this.database.addPropertyChangedListener((componentName, propertyName, propertyValue) -> {
            try {
                componentName = StringUtil.lowercaseFirstLetter(componentName);
                onPropertyValueUpdated.invoke(caller, componentName, propertyName, propertyValue);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        IComponentGroupBootstrap bootstrap = BootstrapFactory.create();
        this.components = bootstrap.start(database, componentGroupOptions, componentsOptions);

        for (IComponent component : this.components.values()) {
            component.addErrorEventListener(componentErrorListener);
        }
    }

    public void setInitialPropertyValue(String componentName, String propertyName, Object propertyValue) {
        componentName = StringUtil.capitalizeFirstLetter(componentName);
        log.info("Setting initial property value to '{}' for property '{}' in component '{}'.", propertyValue, propertyName, componentName);
        this.database.setValue(componentName, propertyName, propertyValue);
    }

    public void setComponentOptions(String componentName, Map<String, Object> options) {
        componentName = StringUtil.capitalizeFirstLetter(componentName);
        log.info("Setting component options for component '{}'.", componentName);
        this.componentsOptions.put(componentName, options);
    }

    public void setPropertyValue(String componentName, String propertyName, Object propertyValue) throws Exception {
        componentName = StringUtil.capitalizeFirstLetter(componentName);
        propertyName = StringUtil.capitalizeFirstLetter(propertyName);

        log.info("Setting property value to '{}' for property '{}' in component '{}'.", propertyValue, propertyName, componentName);

        IComponent component = components.get(componentName);
        component.getClass().getMethod("set" + propertyName, propertyValue.getClass()).invoke(component, propertyValue);
    }

    public void callMethod(String componentName, String methodName, Map<String, Object> parameters) throws Exception {
        componentName = StringUtil.capitalizeFirstLetter(componentName);

        log.info("Calling method '{}' in component '{}'.", methodName, componentName);

        IComponent component = components.get(componentName);

        Reflections reflections = new Reflections(component);
        Set<Method> methods = reflections.getMethodsAnnotatedWith(org.conductor.component.annotations.Method.class);

        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                Parameter[] methodParameters = method.getParameters();

                // We need to sort the parameters, because the incoming parameters is in no specific order.
                Object[] sortedParameters = new Object[methodParameters.length];
                for (int i = 0; i < methodParameters.length; i++) {
                    String parameterName = methodParameters[i].getName();
                    if (!parameters.containsKey(parameterName)) {
                        throw new Exception("Missing parameter '" + parameterName + "'.");
                    }
                    sortedParameters[i] = parameters.get(parameterName);
                }

                method.invoke(component, sortedParameters);
                return;
            }
        }

        throw new Exception("No method was found with the name '" + methodName + "', have you missed adding the annotation to the method?");
    }

    public void setClassLoader(ClassLoader loader) {
        Thread.currentThread().setContextClassLoader(loader);
    }

    public void destroy() {
        for (IComponent component : components.values()) {
            log.info("Destroying component '{}'.", component.getClass().getSimpleName());
            component.destroy();
        }
    }

    ErrorEventListener componentErrorListener = new ErrorEventListener() {

        @Override
        public void onError(IComponent component, String errorMessage, String stackTrace) {
            String componentName = StringUtil.lowercaseFirstLetter(component.getClass().getSimpleName());

            try {
                log.error("Component '{}' reported the error '{}'", componentName, errorMessage);
                reportError.invoke(caller, componentName, errorMessage, stackTrace);
            } catch (Exception e) {
                log.error("Failed to report error to conductor client.", e);
            }
        }
    };

}
