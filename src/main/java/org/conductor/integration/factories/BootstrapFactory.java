package org.conductor.integration.factories;

import org.conductor.bootstrap.Bootstrap;
import org.conductor.bootstrap.IComponentGroupBootstrap;
import org.conductor.component.annotations.ComponentGroupBootstrap;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class BootstrapFactory {

    private static final Logger log = LoggerFactory.getLogger(BootstrapFactory.class);

    public static IComponentGroupBootstrap create() throws Exception {
        List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner(), new TypeAnnotationsScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(""))));

        Set<Class<?>> bootstrappers = reflections.getTypesAnnotatedWith(ComponentGroupBootstrap.class);

        if (bootstrappers.size() > 1) {
            throw new Exception("Found more than one 'ComponentGroupBootstrap'. There can only be zero or one bootstrap defined in each component group project.");
        } else if (bootstrappers.size() == 1) {
            log.info("Found custom component group bootstrapper.");
            Class<?> clsBootstrap = (Class<?>) bootstrappers.toArray()[0];
            return (IComponentGroupBootstrap) clsBootstrap.newInstance();
        } else {
            log.info("No custom component group bootstrapper found. Creating standard component group bootstrapper.");
            return new Bootstrap();
        }
    }
}
