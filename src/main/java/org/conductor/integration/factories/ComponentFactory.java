package org.conductor.integration.factories;

import org.conductor.component.annotations.Component;
import org.conductor.component.types.IComponent;
import org.conductor.database.Database;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class ComponentFactory {
    private Set<Class<?>> components;
    private static String componentsPackage = "";

    public ComponentFactory() {
        List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner(), new TypeAnnotationsScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(""))));

        //sReflections reflections = new Reflections(componentsPackage);
        components = reflections.getTypesAnnotatedWith(Component.class);
    }

    public List<String> getComponentNames() {
        List<String> names = new ArrayList<String>();

        for (Class<?> component : components) {
            names.add(component.getSimpleName());
        }

        return names;
    }

    public IComponent createComponent(String componentName, Database database, Map<String, Object> options) throws InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
        for (Class<?> component : components) {
            if (component.getSimpleName().equals(componentName)) {
                Constructor constructor = component.getConstructor(Database.class, Map.class);
                return (IComponent) constructor.newInstance(database, options);
            }
        }

        return null;
    }
}
