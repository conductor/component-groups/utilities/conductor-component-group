package org.conductor.bootstrap;

import org.conductor.component.types.IComponent;
import org.conductor.database.Database;
import org.conductor.integration.factories.ComponentFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bootstrap implements IComponentGroupBootstrap {

    private static final Logger log = LoggerFactory.getLogger(Bootstrap.class);

    public Map<String, IComponent> start(Database database, Map<String, Object> componentGroupOptions, Map<String, Map<String, Object>> componentsOptions) throws InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
        ComponentFactory componentFactory = new ComponentFactory();
        List<String> componentNames = componentFactory.getComponentNames();
        Map<String, IComponent> components = new HashMap<String, IComponent>();

        log.info("Found {} components.", componentNames.size());

        for (String componentName : componentNames) {
            log.info("Creating component '{}'.", componentName);
            Map<String, Object> componentOption = componentsOptions.get(componentName);
            IComponent component = componentFactory.createComponent(componentName, database, componentOption);
            components.put(componentName, component);
        }

        return components;
    }
}
