package org.conductor.bootstrap;

import java.util.Map;

import org.conductor.component.types.IComponent;
import org.conductor.database.Database;

public interface IComponentGroupBootstrap {
	Map<String, IComponent> start(Database database, Map<String, Object> componentGroupOptions, Map<String, Map<String, Object>> componentsOptions) throws Exception;
}
