package org.conductor.component.annotations;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ComponentOptions.class)
public @interface ComponentOption {
	String name();
	String dataType();
	String defaultValue() default "";
}
