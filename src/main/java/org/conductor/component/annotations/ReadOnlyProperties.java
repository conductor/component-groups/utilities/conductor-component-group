package org.conductor.component.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Henrik on 30/11/2016.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ReadOnlyProperties {
    ReadOnlyProperty[] value();
}
