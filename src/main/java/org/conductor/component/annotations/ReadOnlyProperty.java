package org.conductor.component.annotations;

import org.conductor.component.types.DataType;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Henrik on 30/11/2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ReadOnlyProperties.class)
public @interface ReadOnlyProperty {
    String name();
    DataType dataType();
    String defaultValue();
}
