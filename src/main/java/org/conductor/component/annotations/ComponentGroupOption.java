package org.conductor.component.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ComponentGroupOption {
	String dataType();
	String name();
	String defaultValue() default "";
}
