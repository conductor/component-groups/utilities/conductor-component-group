package org.conductor.component.events;

import org.conductor.component.types.IComponent;

public interface ErrorEventListener {
	void onError(IComponent component, String errorMessage, String stackTrace);
}
