package org.conductor.component.types;

/**
 * Created by Henrik on 30/11/2016.
 */
public enum DataType {
    STRING,
    INTEGER,
    DOUBLE,
    BOOLEAN
}
