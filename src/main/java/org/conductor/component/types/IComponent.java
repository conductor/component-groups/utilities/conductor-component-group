package org.conductor.component.types;

import org.conductor.component.events.ErrorEventListener;

public interface IComponent {
	Object getPropertyValue(String propertyName) throws Exception;
	void updatePropertyValue(String propertyName, Object propertyValue) throws Exception;
	Object getOptionValue(String name);
	void destroy();
	void addErrorEventListener(ErrorEventListener listener);
	void removeErrorEventListener(ErrorEventListener listener);
}
