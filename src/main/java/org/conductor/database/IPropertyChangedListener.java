package org.conductor.database;

public interface IPropertyChangedListener {
	void onPropertyValueChanged(String componentName, String propertyName, Object propertyValue);
}
